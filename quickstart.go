package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	b64 "encoding/base64"

	"golang.org/x/net/context"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	gmail "google.golang.org/api/gmail/v1"
)

// Written by Muhammad Farhan (2020)
// Source =
// - https://developers.google.com/sheets/api/quickstart/go
// - https://github.com/googleapis/google-api-go-client
// - https://developers.google.com/gmail/api/v1/reference/users/messages

// Message object defining email info
type Message struct {
	Size        int64          `json:"size"`
	GmailID     string         `json:"gmail_id"`
	Header      *MessageHeader `json:"header"`
	Snippet     string         `json:"snippet"`
	Attachments []string       `json:"atts"`
}

// MessageHeader object defining header of an email
type MessageHeader struct {
	From    string     `json:"from"`
	Subject string     `json:"subject"`
	Date    *time.Time `json:"date"`
	DateStr string     `json:"date_str"`
}

// Retrieve a token, saves the token, then returns the generated client.
func getClient(config *oauth2.Config) *http.Client {
	// The file token.json stores the user's access and refresh tokens, and is
	// created automatically when the authorization flow completes for the first
	// time.
	tokFile := "token.json"
	tok, err := tokenFromFile(tokFile)
	if err != nil {
		tok = getTokenFromWeb(config)
		saveToken(tokFile, tok)
	}
	return config.Client(context.Background(), tok)
}

// Request a token from the web, then returns the retrieved token.
func getTokenFromWeb(config *oauth2.Config) *oauth2.Token {
	authURL := config.AuthCodeURL("state-token", oauth2.AccessTypeOffline)
	fmt.Printf("Go to the following link in your browser then type the "+
		"authorization code: \n%v\n", authURL)

	var authCode string
	if _, err := fmt.Scan(&authCode); err != nil {
		log.Fatalf("Unable to read authorization code: %v", err)
	}

	tok, err := config.Exchange(context.TODO(), authCode)
	if err != nil {
		log.Fatalf("Unable to retrieve token from web: %v", err)
	}
	return tok
}

// Retrieves a token from a local file.
func tokenFromFile(file string) (*oauth2.Token, error) {
	f, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	tok := &oauth2.Token{}
	err = json.NewDecoder(f).Decode(tok)
	return tok, err
}

// Saves a token to a file path.
func saveToken(path string, token *oauth2.Token) {
	fmt.Printf("Saving credential file to: %s\n", path)
	f, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		log.Fatalf("Unable to cache oauth token: %v", err)
	}
	defer f.Close()
	json.NewEncoder(f).Encode(token)
}

func main() {
	b, err := ioutil.ReadFile("credentials.json")
	if err != nil {
		log.Fatalf("Unable to read client secret file: %v", err)
	}

	// If modifying these scopes, delete your previously saved token.json.
	config, err := google.ConfigFromJSON(b, gmail.GmailReadonlyScope)
	if err != nil {
		log.Fatalf("Unable to parse client secret file to config: %v", err)
	}
	client := getClient(config)

	svc, err := gmail.New(client)
	if err != nil {
		log.Fatalf("Unable to retrieve Gmail client: %v", err)
	}
	err = getMessageList(svc)
	if err != nil {
		log.Fatal(err)
	}
}

// Create directory if not exist
func createDirIfNotExist(dir string) error {
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		err = os.MkdirAll(dir, 0755)
		if err != nil {
			return err
		}
	}
	return nil
}

// Checks if file already exists
func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

// Get list of emails from gmail service
func getMessageList(svc *gmail.Service) error {
	pageToken := ""
	for {
		req := svc.Users.Messages.List("me").Q("from:farhanemir27@gmail.com AND has:attachment AND newer_than:7d")
		if pageToken != "" {
			req.PageToken(pageToken)
		}
		r, err := req.Do()
		if err != nil {
			return err
		}
		log.Printf("Processing %v messages...\n", len(r.Messages))
		err = getMessage(r, svc)
		if err != nil {
			return err
		}
		if r.NextPageToken == "" {
			break
		}
		pageToken = r.NextPageToken
	}
	return nil
}

// Get Message info and attachments from Message List
func getMessage(r *gmail.ListMessagesResponse, svc *gmail.Service) error {
	for _, m := range r.Messages {
		msg, err := svc.Users.Messages.Get("me", m.Id).Do()
		if err != nil {
			return err
		}
		var filenames []string
		headers, err := getMessageHeaders(msg.Payload.Headers)
		if err != nil {
			return err
		}

		folderName := headers.Subject + " - " + headers.DateStr

		err = createDirIfNotExist(folderName)
		if err != nil {
			return err
		}
		if msg.Payload != nil {
			filenames, err = getAttachments(msg.Payload.Parts, folderName, svc, m)
		}
		writeMessageInfo(&Message{
			Size:        msg.SizeEstimate,
			GmailID:     msg.Id,
			Header:      headers,
			Snippet:     msg.Snippet,
			Attachments: filenames,
		}, folderName)
	}
	return nil
}

// Parse date into Time type
func parseDate(date string) (*time.Time, error) {
	day := date[5:7]
	dayInt, err := strconv.Atoi(strings.ReplaceAll(day, " ", ""))
	if err != nil {
		return nil, err
	}
	if dayInt < 10 {
		date = fmt.Sprintf("%s%d%s", date[0:5], 0, date[5:])
	}
	dateParsed, err := time.Parse(time.RFC1123Z, date)
	if err != nil {
		return nil, err
	}
	return &dateParsed, nil
}

// Get attachments from email object and write the attachments
func getAttachments(parts []*gmail.MessagePart, foldername string, svc *gmail.Service, m *gmail.Message) ([]string, error) {
	var filenames []string
	for _, part := range parts {
		if part.Filename != "" {
			filenames = append(filenames, part.Filename)
			filename := fmt.Sprintf("%s/%s", foldername, part.Filename)
			if !fileExists(filename) {
				err := writeAttachments(part, svc, m, filename)
				if err != nil {
					return nil, err
				}
			}
		}
	}
	return filenames, nil
}

// Write attachments into disk
func writeAttachments(part *gmail.MessagePart, svc *gmail.Service, m *gmail.Message, filename string) error {
	attID := part.Body.AttachmentId
	attPart, err := svc.Users.Messages.Attachments.Get("me", m.Id, attID).Do()
	if err != nil {
		return err
	}
	attPartStr := attPart.Data
	base64URL, err := b64.URLEncoding.DecodeString(attPartStr)
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(filename, base64URL, 0644)
	if err != nil {
		return err
	}
	return nil
}

// Get informations from email header
func getMessageHeaders(headers []*gmail.MessagePartHeader) (*MessageHeader, error) {
	headerObj := &MessageHeader{}
	var err error
	for _, h := range headers {
		if h.Name == "Date" {
			headerObj.DateStr = h.Value
			headerObj.Date, err = parseDate(h.Value)
			if err != nil {
				return nil, err
			}
		}
		if h.Name == "Subject" {
			headerObj.Subject = h.Value
		}
		if h.Name == "From" {
			headerObj.From = h.Value
		}
	}
	return headerObj, nil
}

// Write email informations into json file
func writeMessageInfo(messageInfo *Message, folderName string) error {
	file, err := json.MarshalIndent(messageInfo, "", "\t")
	if err != nil {
		return err
	}
	infoJSONName := folderName + "/message_info.json"
	if !fileExists(folderName) {
		err = ioutil.WriteFile(infoJSONName, file, 0644)
		if err != nil {
			return err
		}
	}
	return nil
}
